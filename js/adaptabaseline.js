'use strict';

function adaptBaseline () {
  let baseElem = document.getElementById("base-url");
  let currentUrl = document.URL;
  let onGitLab = currentUrl.includes("cstrobbe.gitlab.io");
  if (!onGitLab) {
    baseElem.parentNode.removeChild(baseElem);
  }
}

adaptBaseline();

